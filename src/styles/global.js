import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  body, html {
    height: 100vh;
    max-width: 100vw;
    font-family: "Poppins", sans-serif;
  }

  #root {
    height: 100vh;
  }
`;

export default GlobalStyle;
