import React from "react";
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider } from '@material-ui/core/styles';
import Routes from "./routes";
import GlobalStyle from './styles/global';


const App = () => <Routes />;

export default App; 