export default function styles() {
  return {
    card: {
      width: '25%',
      minWidth: '275',
      alignContent: 'center',
      borderRadius: 10,
    },
    title: {
      marginTop: 48,
      marginBottom: 48,
      fontSize: 14,
    },
    textFields: {
      marginTop: 12,
      width: '75%',
    },
    enterButton: {
      marginTop: 48,
    },
    forgotPasswordButton: {
      marginTop: 24,
      marginBottom: 48,
    },
    imgLogo: {
      marginTop: 48,
    },
  };
}
  