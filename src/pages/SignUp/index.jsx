import React from 'react';
import { Card, Typography, TextField, Button, Grid, withStyles } from '@material-ui/core';
import styles from './style'

function SignUp({ classes }) {
  return(
      <Grid
        container
        justify="center"
        alignContent="center"
        style={{ alignSelf: "center" }}
      >
        <Card className={classes.card}>
          <Grid container direction="column" alignItems="center">
            <Typography className={classes.title} color="primary" gutterBottom>
              SignIn
            </Typography>
            <TextField className={classes.textFields} label="Usuário" />
            <TextField className={classes.textFields} label="Senha" />
            <Button className={classes.enterButton} variant="contained" size="medium" color="primary">
              Entrar
            </Button>
            <Button className={classes.forgotPasswordButton} size="small" color="primary">
              Forgot your password?
            </Button>
          </Grid>
        </Card> 
      </Grid>
  );
}

export default withStyles(styles)(SignUp);